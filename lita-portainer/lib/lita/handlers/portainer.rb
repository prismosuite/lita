require 'httparty'

module Lita
  module Handlers
    class Portainer < Handler
      class PortainerClient
        include HTTParty

        attr_reader :token

        def initialize(uri)
          self.class.base_uri uri
        end

        def sign_in(username, password)
          resp = self.class.post("/auth", body: { Username: username, Password: password }.to_json,
                                          headers: { 'Content-Type' => 'application/json' })

          @token = resp['jwt']
        end

        def redeploy(id)
          resp = self.class.get("/endpoints/1/docker/containers/#{id}/json", headers: authorized_headers)

          image = resp['Config']['Image'].split(':')
          tag = image.pop
          image = image.join

          Lita.logger.info "Redeploying #{resp['Name']} from #{image}:#{tag} ..."
          resp = create_image(image, tag)

          Lita.logger.info (resp.success?) ? "Container redeployed" : "Container redeploy failed"
        end

        def create_image(image, tag)
          self.class.post("/endpoints/1/docker/images/create?fromImage=#{image}&tag=#{tag}", headers: authorized_headers)
        end

        private

        def authorized_headers
          { 'Content-Type' => 'application/json', 'Authorization' => token }
        end
      end

      config :uri
      config :username
      config :password
      config :web_container_id
      config :sidekiq_container_id

      http.get "/deploy", :deploy

      def deploy(request, response)
        client = PortainerClient.new(config.uri)

        Lita.logger.info "Triggering deploy..."
        client.sign_in(config.username, config.password)
        client.redeploy(config.web_container_id)
        client.redeploy(config.sidekiq_container_id)
        Lita.logger.info "Deploy finished!"
      end

      Lita.register_handler(self)
    end
  end
end
